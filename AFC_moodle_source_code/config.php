<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'mysqli';
$CFG->dblibrary = 'native';
$CFG->dbhost    = '127.0.0.1';
$CFG->dbname    = 'moodle_afc';
$CFG->dbuser    = 'root';
$CFG->dbpass    = '';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbsocket' => 0,
);

$CFG->wwwroot   = 'http://thelearningsite.actionforchildren.org.uk';
$CFG->dataroot  = '/var/moodledata/moodle_afc';
$CFG->admin     = 'admin';

$CFG->directorypermissions = 0777;

$CFG->passwordsaltmain = 't{xSM~eGBF:@&KIQJV2c0AEf;p';
$CFG->sendcoursewelcomemessage=0;
//$CFG->customfrontpageinclude='/home/aurion/source/html/home-v1.html';
require_once(dirname(__FILE__) . '/lib/setup.php');

/******************************************************/
/*Begin                                               */
/******************************************************/
$CFG->staffreferencenumber = 'StaffReferenceNumber';
$CFG->nationalinsurancenumber = 'NationalInsuranceNumber';
$CFG->allstaffaccesscourses = array('Pre-employment Induction Programme'); //courses that everyone can see
$CFG->preinductionstaff = 'Pre-employment Staff'; //staff don't have all courses see permission
/******************************************************/
/*End                                                 */
/******************************************************/
// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
